package com.example.movieapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieAppJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieAppJpaApplication.class, args);
	}
}
