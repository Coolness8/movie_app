package com.example.movieapp.controller;

import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class MovieController {
	
	@RequestMapping(method = RequestMethod.GET)
	public String index(){
		
		return "movie/index";
	}

}
