package com.example.movieapp.model;

import java.util.Date;

public class Movie {

		private String id;
		private String title;
		private String author;
		private int length;
		private Date release_date;
		private String genre;
		private int rate;
		
		
		public Movie() {
			
		}


		public Movie(String id, String title, String author, int length, Date release_date, String genre, int rate) {
			
			this.id = id;
			this.title = title;
			this.author = author;
			this.length = length;
			this.release_date = release_date;
			this.genre = genre;
			this.rate = rate;
		}


		public String getId() {
			return id;
		}


		public void setId(String id) {
			this.id = id;
		}


		public String getTitle() {
			return title;
		}


		public void setTitle(String title) {
			this.title = title;
		}


		public String getAuthor() {
			return author;
		}


		public void setAuthor(String author) {
			this.author = author;
		}


		public int getLength() {
			return length;
		}


		public void setLength(int length) {
			this.length = length;
		}


		public Date getRelease_date() {
			return release_date;
		}


		public void setRelease_date(Date release_date) {
			this.release_date = release_date;
		}


		public String getGenre() {
			return genre;
		}


		public void setGenre(String genre) {
			this.genre = genre;
		}


		public int getRate() {
			return rate;
		}


		public void setRate(int rate) {
			this.rate = rate;
		}
		
		
		
		
}
